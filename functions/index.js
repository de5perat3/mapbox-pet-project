const functions = require('firebase-functions');
const axios = require('axios');
const admin = require('firebase-admin');
const cors = require('cors')();
const serviceAccount = require('./mapbox-pet-firebase-adminsdk-ax2wx-df5b7b5b33');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://mapbox-pet.firebaseio.com'
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
const validatePoint = () => {
  // free tier unllowed to do other domains requests
  const validationAPI = 'https://www.random.org/integers/?num=1&min=1&max=5&col=1&base=10&format=plain&rnd=new';

  return axios.get(validationAPI)
    .then(res => res.data);
}
const validateCordinates = body => {
  if(!body || !body.coordinates) {
    return false;
  }

  const [lat, lng] = body.coordinates;

  return lat && lng && typeof lat === 'number' && typeof lng === 'number';
}

const pointsRef = admin.database().ref('/points');

exports.saveMapPoint = functions.https.onRequest(async (request, response) => {
  cors(request, response, async () => {
    const { method, body } = request;

    if(method !== 'POST'){
      return response.status(400);
    }
    try {
      const pointValue = await validatePoint();
      const validCoordinates = validateCordinates(body);
  
      if (Number(pointValue) === 5 || !validCoordinates) {
        return response.status(400).json({ message: 'Validation failed' });
      }
  
      await pointsRef.push({coordinates: body.coordinates, pointValue});
        
      return response.json({ message: 'Successfully added' });
    } catch (e) {
      return response.json({ message: 'Validation failed', error: e.message });
    }
    
  });
  
});

exports.deleteMapPoint = functions.https.onRequest(async (request, response) => {
  cors(request, response, async () => {
    const { method, query } = request;
    const { id } = query;

    if(method !== 'DELETE'){
      return response.status(400);
    }

    if(!id) {
      return response.status(400).json({ message: 'Missing id field', body });
    }

    try {
      await pointsRef.child(id).remove();

      return response.json({ message: 'Successfully removed' });
    } catch(e) {
      return response.status(400).json({ message: `Request failed: ${e.message}` });
    }
  });
});
