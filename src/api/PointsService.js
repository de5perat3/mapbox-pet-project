import axios from 'axios';

const API_URL = process.env.apiURL || 'https://us-central1-mapbox-pet.cloudfunctions.net';

const savePoint = (coordinates) => {
  if(!coordinates) {
    return Promise.reject('Missing required coordinates.')
  }
  const savePointURL = API_URL + '/saveMapPoint';

  return axios.post(savePointURL, {coordinates});
}

const deletePoint = id => {
  if(!id) {
    return Promise.reject('Missing required id.')
  }
  const deletePointURL = `${API_URL}/deleteMapPoint?id=${id}`;

  return axios.delete(deletePointURL);
}

export { savePoint, deletePoint }; 
