export const generateDeleteButton = attr => {
  return `<div style="margin-top:10px">
    <button id="popup-marker-delete" style="color:red" data-id="${attr}">X</button>
  </div>`;
}

export const markerMixins = {
  data() {
    return {
      markers: [
        {value: 1, color: 'gray', label: 'One'},
        {value: 2, color: 'red', label: 'Two'},
        {value: 3, color: 'orange', label: 'Three'},
        {value: 4, color: 'lime', label: 'Four'}
      ]
    }
  }
}
