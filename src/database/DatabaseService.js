// database service to use basic CRUD operation
import DatabaseInterface from './DatabaseInterface';

export default class DatabaseService extends DatabaseInterface {
  constructor(db) {
    super();
    this.db = db;
  }

  listenForValues(cb = () => {}) {
    const pointsRef = this.db.ref('/points');

    pointsRef.on('child_added', (snapshot) => {
      const id = snapshot.key
      const pointData = snapshot.val();
      const newPoint = Object.assign({}, pointData, {id})

      cb(newPoint);
    });
  }

}