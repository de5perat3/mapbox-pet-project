import firebase from 'firebase/app'
import 'firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyBLFFYlosRpWDk_n63W5EuXkHraI_ryLRs",
  authDomain: "mapbox-pet.firebaseapp.com",
  databaseURL: "https://mapbox-pet.firebaseio.com",
  projectId: "mapbox-pet",
  storageBucket: "mapbox-pet.appspot.com",
  messagingSenderId: "350040955368",
  appId: "1:350040955368:web:8ca9f4ff2bbb5dbdb76c5d"
};

firebase.initializeApp(firebaseConfig);

export const firebaseRealtimeDB = firebase.database();