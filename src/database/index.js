import { firebaseRealtimeDB } from './firebaseConfig';
import DatabaseService from './DatabaseService';

export const databaseService = new DatabaseService(firebaseRealtimeDB);
