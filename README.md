# Mapbox - Vue.js

## Demo - https://mapbox-pet.firebaseapp.com/


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# cloud functions
cd function
npm install


```
